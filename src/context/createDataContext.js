import React, { useReducer } from "react";

export default (reducer, actions, initialState) => {
  const ConText = React.createContext();
  const Provider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    //action === {addBlogPost: (dispatch) => {return () => {}}}
    const boundActions = {};
    for (let key in actions) {
      boundActions[key] = actions[key](dispatch);
    }
    return (
      <ConText.Provider value={{ state, ...boundActions }}>
        {children}
      </ConText.Provider>
    );
  };

  return { ConText, Provider };
};
